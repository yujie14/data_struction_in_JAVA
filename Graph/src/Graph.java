import java.util.*;
public class Graph 
{
	int numOfNode;
	int[] visited;
	char[] vex;
	int[] in;
	int[][] arc;
	
	public Graph(char[] vex, int[][]arc)
	{
		this.numOfNode = vex.length;
		this.visited = new int[this.numOfNode];
		this.in = new int[this.numOfNode];       
		this.vex = vex;
		this.arc = arc;
	    for(int i = 0; i < this.numOfNode; i++)
	        for(int j = 0; j < this.numOfNode; j++)
	            if(arc[i][j] != 0)
	                this.in[j]++;
	}
	
	
	public void tuopo()     //拓扑排序 邻接矩阵
	{
		List<Integer> q = new ArrayList<Integer>();
		int zhi;
	    for(int i = 0; i < this.numOfNode; i++)
	        if(this.in[i] == 0)
	            q.add(i);
	    while(!q.isEmpty())
	    {
	        zhi = (int)q.get(0);
	        q.remove(0);
	        System.out.print(this.vex[zhi] + " - ");
	        for(int i = 0; i < this.numOfNode; i++)
	            if(arc[zhi][i] != 0)
	            {
	                this.in[i]--;
	                if(this.in[i] == 0)
	                	q.add(i);
	            }
	    }
	    System.out.println();
	}
	
	
	public void BFS()                  //深度优先 邻接矩阵       
	{
	    int zhi;
	    List<Integer> q = new ArrayList<Integer>();
	    for(int i = 0; i < this.numOfNode; i++) 
	    	this.visited[i]=0;
	    q.add(0);
	    this.visited[0]=1;
	    while(!q.isEmpty())
	    {
	        zhi = (int)q.get(0);
	        q.remove(0);
	        System.out.print(this.vex[zhi] + " - ");
	        for(int i = 0; i < this.numOfNode; i++)
	            if(arc[zhi][i] != 0 && this.visited[i] == 0)
	            {
	            	q.add(i);
	                this.visited[i]=1;
	            }
	    }
	    System.out.println();
	} 
	
	
	void Dfs(int a)                     //广度优先 邻接矩阵
	{
		System.out.print(this.vex[a] + " - ");
	    this.visited[a]=1;
	    for(int i = 0; i < this.numOfNode; i++)
	        if(this.arc[a][i] != 0 && this.visited[i] == 0)
	            Dfs(i);
	}
	void DFS()
	{
	    for(int i = 0; i < this.numOfNode; i++)
	        this.visited[i] = 0;
	    for(int i = 0; i < this.numOfNode; i++)
	        if(this.visited[i] == 0)
	            Dfs(i);
	    System.out.println();
	}  
	
	
	public void MiniSpanTree_Prim() // 最小生成树 普里姆算法
	{
		int[][] arcLocal = new int[arc.length][arc[0].length];
		for(int i = 0; i < arc.length; i++)
			for(int j = 0; j < arc[0].length; j++)
			{
				if(this.arc[i][j] != 0)
					arcLocal[i][j] = this.arc[i][j];
				else if(i == j)
						arcLocal[i][j] = 0;
				else
					arcLocal[i][j] = 10000;
			}
	    char[] adjvex = new char[this.numOfNode];
	    int[] lowcost = new int[this.numOfNode];
	    adjvex[0] = vex[0];
	    lowcost[0] = 0;
	    for(int i = 1; i < this.numOfNode; i++)
	    {
	        adjvex[i] = 'A';
	        lowcost[i] = arcLocal[0][i];
	    }
	    for(int i = 1; i < this.numOfNode; i++)
	    {
	        int j = 1, k = 0, min = 10000;
	        while(j < this.numOfNode)
	        {
	            if(lowcost[j] != 0 && lowcost[j] < min)
	            {
	                min = lowcost[j];
	                k = j;
	            }
	            j++;
	        }
	        System.out.print("(" + adjvex[k] + ", " + vex[k] + ")" + min + "    ");
	        lowcost[k] = 0;
	        for(j = 1; j < this.numOfNode; j++)
	        {
	            if(lowcost[j] != 0 && arcLocal[k][j] < lowcost[j])
	            {
	                lowcost[j] = arcLocal[k][j];
	                adjvex[j]= this.vex[k];
	            }
	        }
	    }
	    System.out.println();
	}
	
	
	public void MiniSpanTree_Kruskal() // 最小生成树 克鲁斯卡尔算法
	{
		List<String> edge = new ArrayList<String>();
		for(int i = 0; i < this.arc.length; i++)
			for(int j = 0; j < this.arc[0].length; j++)
				if(this.arc[i][j] != 0)
					edge.add(vex[i] + " " + vex[j] + " " + this.arc[i][j]);
		Comparator<String> c = new Comparator<String>()
				{
			            public int compare(String s1, String s2)
			            {
			            	String[] ss1 = s1.split(" ");
			            	String[] ss2 = s2.split(" ");
			            	int o1 = Integer.parseInt(ss1[2]);
			            	int o2 = Integer.parseInt(ss2[2]);
			            	if(o1 < o2)
			            		return -1;
			            	else if(o1 == o2)
			            		return 0;
			            	else
			            		return 1;
			            }
				};
		Collections.sort(edge, c);
		char p, q;
		char[] parent = new char[edge.size()-1];
		for(int i = 0; i < edge.size()-1; i++)
	        parent[i]=',';
		for(int i = 0; i < edge.size() - 1; i++)
	    {
			String[] s = edge.get(i).split(" ");
	        p=find(parent, s[0].charAt(0));
	        q=find(parent, s[1].charAt(0));
	        if(p != q)
	        {
	            parent[p-'A'] = q;
	            System.out.print("(" + s[0] + ", " + s[1] + ")" + s[2] + "    ");
	        }
	    }	
		System.out.println();
	}
	public char find(char[] parent, char a)
	{
		while(parent[a - 'A'] != ',')
			a = parent[a-'A'];
		return a;
	}
	
	
	public void ShortestPath_Dijkstra(char A, char B)   // 最短路径 迪杰斯特拉算法
	{
		int a = A - 'A';
		int b = B - 'A';
		int[][] arcLocal = new int[arc.length][arc[0].length];
		for(int i = 0; i < arc.length; i++)
			for(int j = 0; j < arc[0].length; j++)
			{
				if(this.arc[i][j] != 0)
					arcLocal[i][j] = this.arc[i][j];
				else if(i == j)
						arcLocal[i][j] = 0;
				else
					arcLocal[i][j] = 10000;
			}
		int min, l = 0;
		int[] isVisited = new int[this.numOfNode];
		int[] d = new int[this.numOfNode];
		int[] p = new int[this.numOfNode];
		for(int i = 0; i < this.numOfNode; i++)
		{
			d[i] = arcLocal[a][i];
			p[i] = a;
		}
		d[a] = 0;
		isVisited[a] = 1;
		for(int i = 1; i < this.numOfNode; i++)
		{
			min = 10000;
			for(int j = 0; j < this.numOfNode; j++)
				if(isVisited[j] == 0 && d[j] < min)
				{
					min = d[j];
					l = j;
				}
			isVisited[l] = 1;
			for(int j = 0; j < this.numOfNode; j++)
				if(isVisited[j] == 0 && min + arcLocal[l][j] < d[j])
				{
					d[j] = min + arcLocal[l][j];
					p[j] = l;
				}
		}
		if(d[b]!=10000)
	    {
	        System.out.print((char)(b + 'A') + "<--");
	        while((p[b]) != a)
	        {
	        	System.out.print((char)(p[b] + 'A') + "<--");
	            b = p[b];
	        }
	        System.out.print((char)(p[a] + 'A'));
	    }
		else
			System.out.print(-1);
		System.out.println();
	}
	
	
	public void ShortestPath_Floyd(char A, char B)   // 最短路径 弗洛伊德算法
	{
		int a = A - 'A';
		int b = B - 'A';
		int[][] arcLocal = new int[arc.length][arc[0].length];
		for(int i = 0; i < arc.length; i++)
			for(int j = 0; j < arc[0].length; j++)
			{
				if(this.arc[i][j] != 0)
					arcLocal[i][j] = this.arc[i][j];
				else if(i == j)
						arcLocal[i][j] = 0;
				else
					arcLocal[i][j] = 10000;
			}
		int[][] d = new int[this.numOfNode][this.numOfNode];
		int[][] p = new int[this.numOfNode][this.numOfNode];
		for(int i = 0; i < this.numOfNode; i++)
			for(int j = 0; j < this.numOfNode; j++)
			{
				d[i][j] = arcLocal[i][j];
				p[i][j] = j;
			}
		for(int i = 0; i < this.numOfNode; i++)
			for(int j = 0; j < this.numOfNode; j++)
				for(int k = 0; k < this.numOfNode; k++)
					if(d[i][j] > d[i][k] + d[k][j])
					{
						d[i][j] = d[i][k] + d[k][j];
						p[i][j] = p[i][k];
					}
		if(d[a][b]!=10000)
	    {
	        System.out.print((char)(a + 'A') + "-->");
	        while((p[a][b]) != b)
	        {
	        	System.out.print((char)(p[a][b] + 'A') + "-->");
	            a = p[a][b];
	        }
	        System.out.print((char)(b + 'A'));
	    }
		else
			System.out.print(-1);
		System.out.println();

	}
	
}
