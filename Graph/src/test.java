public class test 
{
	public static void main(String[] args) 
	{
		char vex[] = {'A','B','C','D','E','F','G','H','I','J','K'};
	    int arc[][] = {{0, 1, 0, 0, 4, 0, 0, 6, 0, 0, 0},{0, 0, 2, 0, 0, 2, 0, 0, 0, 0, 0},
	    		{0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0},{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4},
	    		{0, 3, 0, 0, 0, 3, 0, 0, 0, 0, 0},{0, 0, 0, 2, 0, 0, 3, 0, 0, 3, 0},
	    		{0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 3},{0, 0, 0, 0, 2, 1, 0, 0, 6, 0, 0},
	    		{0, 0, 0, 0, 0, 2, 0, 0, 0, 6, 0},{0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 4},
	    		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};
	    Graph p = new Graph(vex, arc);
	    p.MiniSpanTree_Prim();
	    p.MiniSpanTree_Kruskal();
	    for(int i = 0; i < 11; i++)
	    	for(int j = 0; j < 11; j++)
	    	{
	    		p.ShortestPath_Dijkstra((char)(i+'A'),(char)(j+'A'));
	    		p.ShortestPath_Floyd((char)(i+'A'),(char)(j+'A'));
	    	}
	    		
	}

}
