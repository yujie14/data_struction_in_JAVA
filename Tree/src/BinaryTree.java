import java.util.*;
public class BinaryTree<E extends Comparable<E>>
{
	private TreeNode<E> root;
	private int size = 0;
	public BinaryTree()
	{
	}
	
	public boolean insert(E val)
	{
		TreeNode<E> p = new TreeNode<E>(val);
		if(this.root == null)
		{
			this.root = p;
			return true;
		}
		else
		{
			TreeNode<E> parent = null;
			TreeNode<E> current = this.root;
			while(current != null)
			{
				if(val.compareTo(current.val) < 0)
				{
					parent = current;
					current = current.left;
				}
				else if(val.compareTo(current.val) > 0)
				{
					parent = current;
					current = current.right;
				}
				else
					return false;
			}
			if(val.compareTo(parent.val) < 0)
				parent.left = p;
			else
				parent.right = p;
		}
		this.size++;
		return true;	
	}
	
	public boolean delete(E val)
	{
		TreeNode<E> parent = null;
		TreeNode<E> current = root;
		while(current != null)
		{
			if(val.compareTo(current.val) < 0)
			{
				parent = current;
				current = current.left;
			}
			else if(val.compareTo(current.val) > 0)
			{
				parent = current;
				current = current.right;
			}
			else
				break;
		}
		if(current == null)
			return false;
		if(current.left == null)
		{
			if(parent == null)
				root = current.right;
			else if(val.compareTo(parent.val) < 0)
				parent.left = current.right;
			else
				parent.right = current.right;
		}
		else
		{
			TreeNode<E> parentOfRightMost = current;
			TreeNode<E> rightMost = current.left;
			while(rightMost.right != null)
			{
				parentOfRightMost = rightMost;
				rightMost = rightMost.right;
			}
			current.val = rightMost.val;
			if(parentOfRightMost.right == rightMost)
				parentOfRightMost.right = rightMost.left;
			else
				parentOfRightMost.left = rightMost.left;
		}
		this.size--;
		return true;
	}
	
	public int getSize()
	{
		return this.size;
	}
	
	public TreeNode<E> getRoot()
	{
		return this.root;
	}

	public void inorder()
	{
		this.inorder(this.root);
	}
	
	private void inorder(TreeNode<E> node)
	{
		if(node == null)
			return;
		inorder(node.left);
		System.out.print(node.val + " ");
		inorder(node.right);
	}
	
	public void preorder()
	{
		this.preorder(this.root);
	}
	
	private void preorder(TreeNode<E> node)
	{
		if(node == null)
			return;
		System.out.print(node.val + " ");
		preorder(node.left);
		preorder(node.right);
	}
	
	public void postorder()
	{
		this.postorder(this.root);
	}
	
	private void postorder(TreeNode<E> node)
	{
		if(node == null)
			return;
		postorder(node.left);
		postorder(node.right);
		System.out.print(node.val + " ");
	}
	
	public void levelorder()
	{
		if(root==null)
			return;
		ArrayList<TreeNode<E>> list = new ArrayList<TreeNode<E>>();  
	    list.add(root);
	    TreeNode<E> currentNode;
	    while(!list.isEmpty())
	    {
	    	currentNode=list.get(0);
	    	list.remove(0);
	    	System.out.print(currentNode.val + " ");
	        if(currentNode.left!=null)
	        	list.add(currentNode.left);
	        if(currentNode.right!=null)
	            list.add(currentNode.right);
	     }
	}
	
	public List<E> path(E val)
	{
		List<E> l = new ArrayList<E>();
		TreeNode<E> current = this.root;
		while(current != null)
		{
			l.add(current.val);
			if(val.compareTo(current.val) < 0)
				current = current.left;
			else if(val.compareTo(current.val) > 0)
				current = current.right;
			else
				break;
		}
		return l;
	}
	
	public class TreeNode<E>
	{
		E val;
		TreeNode<E> left;
		TreeNode<E> right;
	    public TreeNode(E val) 
		{
			this.val = val;
		}
	}
}
