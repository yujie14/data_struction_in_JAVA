public class StringMatch 
{
	public void getNext(String str, int[] next)
	{
		int i = 1;
		int j = 0;
		next[1] = 0;
		while(i < str.length())
		{
			if(j == 0 || str.charAt(i) == str.charAt(j))
			{
				i++;
				j++;
				next[i] = j;
			}
			else
				j = next[j];
		}
	}
	
	public int Index_KMP(String S, String T)
	{
		S = " " + S;
		T = " " + T;
		int i = 1;
		int j = 1;
		int[] next = new int[T.length()+1];
		this.getNext(T, next);
		while(i < S.length() && j < T.length())
		{
			if(j == 0 || S.charAt(i) == T.charAt(j))
			{
				i++;
				j++;
			}
			else
				j = next[j];
		}
		if(j == T.length())
			return i - T.length();
		return -1;
	}
}
