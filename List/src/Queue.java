public class Queue 
{
	ListNode first;
	ListNode last;
	int count;
	
	public Queue()
	{
		this.count = 0;
	}
	
	public boolean EnQueue(int val)
	{
		ListNode p = new ListNode(val);
		if(this.count == 0)
		{
			this.first = p;
			this.last = p;
			this.count++;
			return true;
		}
		this.last.next = p;
		this.last = p;
		this.count++;
		return true;
	}
	
	public boolean DeQueue()
	{
		if(this.count == 0)
			return false;
		this.first = this.first.next;
		this.count--;
		return true;
	}
	
	
	
	public boolean clear()
	{
		if(this.first == null)
			return false;
		this.first = null;
		this.last = null;
		return true;
	}
	
	public boolean isEmpty()
	{
		if(this.first == null)
			return true;
		return false;
	}
	
	public int
	getFirst()
	{
		if(this.first == null)
			return -1;
		return this.first.val;
	}
	
	public int getLength()
	{
		return this.count;
	}
	
	public void show()
	{
		if(this.first == null)
		{
			System.out.println("null");
			return;
		}
		ListNode p = this.first;
		while(p.next != null)
		{
			System.out.print(p.val + " ---> ");
			p = p.next;
		}
		System.out.println(p.val);
	}
	
	public class ListNode   //internal class
	{
		int val;
		ListNode next;
		ListNode(int val) 
		{
			this.val = val;
		}
	}
}
