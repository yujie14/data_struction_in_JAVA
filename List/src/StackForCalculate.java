public class StackForCalculate 
{
	public double RPN(String str)
	{
		Stack<Double> l = new Stack<Double>();
		l.clear();
		double result = 0, first, second;
		String s[] = str.split(" ");
		for(int i = 0; i < s.length; i++)
		{
			if((s[i].charAt(0)-'0') >= 0 && (s[i].charAt(0)-'0') <= 9)
				l.push((double)Integer.parseInt(s[i]));
			else
			{
				second = l.top();
				l.pop();
				first = l.top();
				l.pop();
				if(s[i].equals("+"))
				{
					result = first + second;
				}
				if(s[i].equals("-"))
				{
					result = first - second;
				}
				if(s[i].equals("*"))
				{
					result = first * second;
				}
				if(s[i].equals("/"))
				{
					result = first / second;
				}
				l.push(result);
			}
		}
		return l.top();
	}
	
	public String InfixToPostfix(String str)
	{
		Stack<Character> c = new Stack<Character>();
		c.clear();
		char a, b;
		String[] s = str.split(" ");
		String result = "";
		for(int i = 0; i < s.length; i++)
		{
			a = s[i].charAt(0);
			if(a >= '0' && a <= '9')
				result = result + s[i] + " ";
			else if(a == '*' || a == '/' || a == '(')
				c.push(a);
			else if(a == ')')
			{
				b = c.top();
				c.pop();
                while(b != '(')
                {
                	result = result + b + " ";
                	b = c.top();
                	c.pop();
                }
             }
			else if(a == '+' || a == '-')
			{
				if(c.isEmpty())
					c.push(a);
				else
				{
					do
					{
						b = c.top();
	                	c.pop();
                        if(b=='(')
                        	c.push(b);
                        else
                        	result = result + b + " ";
                    } while(!c.isEmpty() && b != '(');
                    c.push(a);
                }
            }
         }
    while(!c.isEmpty())
    {
        b = c.top();
        c.pop();
        result = result + b + " ";
    }
    return result;
	}
}
