public class Stack<E> 
{
	ListNode<E> top;
	int count;
	public Stack()
	{
		this.count = 0;
	}
	
	public boolean push(E val)
	{
		ListNode<E> p = new ListNode<E>(val);
		if(this.top == null)
		{
			this.top = p;
			this.count++;
			return true;
		}
		p.next = this.top;
		this.top = p;
		this.count++;
		return true;
	}
	
	public boolean pop()
	{
		if(this.top == null)
			return false;
		this.count--;
		this.top = this.top.next;
		return true;	
	}
	
	public boolean clear()
	{
		if(this.top == null)
			return false;
		this.top = null;
		return true;
	}
	
	public boolean isEmpty()
	{
		if(this.top == null)
			return true;
		return false;
	}
	
	public E top()
	{
		if(this.top == null)
			return null;
		return this.top.val;
	}
	
	public int getLength()
	{
		return this.count;
	}
	
	public void show()
	{
		if(this.top == null)
		{
			System.out.println("null");
			return;
		}
		ListNode<E> p = this.top;
		while(p.next != null)
		{
			System.out.print(p.val + " ---> ");
			p = p.next;
		}
		System.out.println(p.val);
	}
	
	public class ListNode<E>   //internal class
	{
		E val;
		ListNode<E> next;
		ListNode(E val) 
		{
			this.val = val;
		}
	}
}
