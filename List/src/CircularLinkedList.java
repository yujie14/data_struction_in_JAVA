public class CircularLinkedList<E>
{
	ListNode<E> head;
	public CircularLinkedList(E val)
	{
		this.head = new ListNode<E>(val);
		this.head.next = this.head;
	}
	
	
	public int length()
	{
		if(this.head == null)
			return 0;
		ListNode<E> p = this.head.next;
		int len = 1;
		while(p != this.head)
		{
			p = p.next;
			len++;
		}
		return len;
	}
	
	public ListNode<E> get(int index)
	{
		ListNode<E> p = null;
		if(index < this.length())
		{
			p = this.head;
			for(int i = 0; i < index; i++)
				p = p.next;
		}
		return p;
	}
	
	public int indexOf(E val)
	{
		if(this.head == null)
			return -1;
		ListNode<E> p = this.head;
		int count = 0;
		while(p.val != val && count < length()-1)
		{
			p = p.next;
			count++;
		}
		return p.val != val ? -1 : count;
	}
	
	public boolean insert(int index, E val)
	{
		ListNode<E> ln = new ListNode<E>(val);
		if(this.head == null && index == 0)
		{
			this.head = ln;
			ln.next = ln;
			return true;
		}
		if(index == 0)
		{
			ln.next = this.head;
			this.head = ln;
			return true;
		}
		if(index > this.length() || index < 0)
			return false;
		ListNode<E> p = this.head;
		int count = 1;
		while(count < index)
		{
			p = p.next;
			count++;
		}
		ln.next = p.next;
		p.next = ln;
		return true;
	}
	
	public boolean delete(int index)
	{
		if(this.head == null)
			return false;
		if(this.length()==1 && index == 0)
		{
			this.head = null;
			return true;
		}
		if(index == 0)
		{
			this.head = this.head.next;
			return true;
		}
		if(index >= this.length() || index < 0)
			return false;
		ListNode<E> p = this.head;
		int count = 1;
		while(count < index)
		{
			p = p.next;
			count++;
		}
		p.next = p.next.next;
		return true;
	}
	
	public boolean set(int index, E val)
	{
		if(index < this.length() && index >= 0)
		{
			ListNode<E> p = this.head;
			for(int i = 0; i < index; i++)
				p = p.next;
			p.val = val;
			return true;
		}
		return false;
	}
	
	public void add(E val)
	{
		ListNode<E> ln = new ListNode<E>(val);
		if(this.head == null)
		{
			this.head = ln;
			this.head.next = this.head;
			return;
		}
		ListNode<E> p = this.head;
		int count = 0;
		while(count < length()-1)
		{
			p = p.next;
			count++;
		}
		ln.next = p.next;
		p.next = ln;	
	}
	
	public E last()
	{
		if(this.head == null)
			return null;
		ListNode<E> p = this.head;
		int count = 0;
		while(count < length()-1)
		{
			p = p.next;
			count++;
		}
		return p.val;
	}
	
	public void show(int from)
	{
		if(this.head == null)
		{
			System.out.println("null");
			return;
		}
		while(from < 0)
			from = from + length();
		ListNode<E> p = this.head;
		for(int i = 0; i < from; i++)
			p = p.next;
		int count = 0;
		while(count < length()-1)
		{
			System.out.print(p.val + " ---> ");
			p = p.next;
			count++;
		}
		System.out.println(p.val);
	}
	
	public void show()
	{
		if(this.head == null)
		{
			System.out.println("null");
			return;
		}
		ListNode<E> p = this.head;
		int count = 0;
		while(count < length()-1)
		{
			System.out.print(p.val + " ---> ");
			p = p.next;
			count++;
		}
		System.out.println(p.val);
	}
	
	public class ListNode<E>   //internal class
	{
		E val;
		ListNode<E> next;
		ListNode (E val) 
		{
			this.val = val;
		}
	}
}
