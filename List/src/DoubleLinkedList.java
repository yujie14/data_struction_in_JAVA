public class DoubleLinkedList<E> 
{
	ListNode<E> head;
	ListNode<E> tail;
	public DoubleLinkedList(E val)
	{
		this.head = new ListNode<E>(val);
		this.tail = this.head;
	}
	
	
	public int length()
	{
		if(this.head == null)
			return 0;
		ListNode<E> p = this.head;
		int len = 1;
		while(p != this.tail)
		{
			p = p.next;
			len++;
		}
		return len;
	}
	
	public ListNode<E> get(int index)
	{
		ListNode<E> p = null;
		if(index < this.length())
		{
			p = this.head;
			for(int i = 0; i < index; i++)
				p = p.next;
		}
		return p;
	}
	
	public int indexOf(E val)
	{
		if(this.head == null)
			return -1;
		ListNode<E> p = this.head;
		int count = 0;
		while(p.val != val && count < length()-1)
		{
			p = p.next;
			count++;
		}
		return p.val != val ? null : count;
	}
	
	public boolean insert(int index, E val)
	{
		ListNode<E> ln = new ListNode<E>(val);
		if(this.head == null && index == 0)
		{
			this.head = ln;
			this.tail = ln;
			return true;
		}
		if(index == 0)
		{
			ln.next = this.head;
			this.head.previous = ln;
			this.head = ln;
			return true;
		}
		if(index == this.length())
		{
			ln.previous = this.tail;
			this.tail.next = ln;
			this.tail = ln;
			return true;
		}
		if(index > this.length() || index < 0)
			return false;
		ListNode<E> p = this.head;
		int count = 1;
		while(count < index)
		{
			p = p.next;
			count++;
		}
		ln.previous = p;
		ln.next = p.next;
		p.next.previous = ln;
		p.next = ln;
		return true;
	}
	
	public boolean delete(int index)
	{
		if(this.head == null)
			return false;
		if(this.length()==1 && index == 0)
		{
			this.head = null;
			this.tail = null;
			return true;
		}
		if(index == 0)
		{
			this.head = this.head.next;
			this.head.previous = null;
			return true;
		}
		if(index == this.length()-1)
		{
			this.tail = this.tail.previous;
			this.tail.next = null;
			return true;
		}
		if(index >= this.length() || index < 0)
			return false;
		ListNode<E> p = this.head;
		int count = 1;
		while(count < index)
		{
			p = p.next;
			count++;
		}
		p.next = p.next.next;
		p.next.previous = p;
		return true;
	}
	
	public boolean set(int index, E val)
	{
		if(index < this.length() && index >= 0)
		{
			ListNode<E> p = this.head;
			for(int i = 0; i < index; i++)
				p = p.next;
			p.val = val;
			return true;
		}
		return false;
	}
	
	public void addStart(E val)
	{
		ListNode<E> ln = new ListNode<E>(val);
		ln.next = this.head;
		this.head.previous = ln;
		this.head = ln;
	}
	
	public void addEnd(E val)
	{
		ListNode<E> ln = new ListNode<E>(val);
		ln.previous = this.tail;
		this.tail.next = ln;
		this.tail = ln;
	}
	
	public E last()
	{
		if(this.tail == null)
			return null;
		return this.tail.val;
	}
	
	public void showStart()
	{
		if(this.head == null)
		{
			System.out.println("null");
			return;
		}
		ListNode<E> p = this.head;
		while(p.next != null)
		{
			System.out.print(p.val + " ---> ");
			p = p.next;
		}
		System.out.println(p.val);
	}
	
	public void showEnd()
	{
		if(this.tail == null)
		{
			System.out.println("null");
			return;
		}
		ListNode<E> p = this.tail;
		while(p.previous != null)
		{
			System.out.print(p.val + " ---> ");
			p = p.previous;
		}
		System.out.println(p.val);
	}
	
	public class ListNode<E>   //internal class
	{
		E val;
		ListNode<E> next;
		ListNode<E> previous;
		ListNode(E val) 
		{
			this.val = val;
		}
	}
}
