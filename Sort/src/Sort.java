public class Sort
{
	// 交换排序
	public void Bubble_Sort(double[] e)                 // 冒泡排序算法
	{
		for(int i = 0; i < e.length-1; i++)
			for(int j = e.length-1; j > i; j--)
				if(e[j-1] > e[j])
					this.exchange(j-1, j, e);
	}
	
	public void Improved_Bubble_Sort(double[] e)        // 冒泡排序改进算法
	{
		boolean flag = true;
		for(int i = 0; i < e.length-1 && flag; i++)
		{
			flag = false;
			for(int j = e.length-1; j > i; j--)
				if(e[j-1] > e[j])
				{
					flag = true;
					this.exchange(j-1, j, e);
				}
		}
	}
	
	public void Quick_Sort(double[] e)                  // 快速排序算法
	{
		this.QS(e, 0, e.length-1);
	}
	public void QS(double e[], int left, int right)
	{
	    if(left < right)
	    {
	        int i, j;
	        double pivot = median(e, left, right);
	        i=left;
	        j=right-1;
	        while(true)
	        {
	            while(e[i]<=pivot)   i++;
	            while(e[j]>=pivot)   j--;
	            if(i<j)
	            	this.exchange(i, j, e);
	            else
	                break;
	        }
	        this.exchange(i, right, e);
	        QS(e,left,i-1);
	        QS(e,i+1,right);
	    }
	}
	public double median(double e[], int left, int right)
	{
	    int center;
	    center = ( left + right ) / 2;
	    if(e[left]>e[center])
	        this.exchange(left, center, e);
	    if(e[left]>e[right])
	    	this.exchange(left, right, e);
	    if(e[center]>e[right])
	    	this.exchange(center, right, e);
	    this.exchange(center, right, e);
	    return e[right];
	}
	
	
	// 选择排序
	public void Simple_Selection_Sort(double[] e)       // 简单选择排序算法
	{
		int min;
		for(int i = 0; i < e.length-1; i++)
		{
			min = i;
			for(int j = i; j < e.length; j++)
				if(e[j] < e[min])
					min = j;
			if(min != i)
				this.exchange(min, i, e);
		}
	}
	
	public void Heap_Sort(double[] e)                  // 堆排序算法
	{
	    for(int i = e.length / 2; i > 0; i--)
	        this.HeapAdjust(e, i, e.length);
	    for(int i = e.length; i > 1; i--)
	    {
	        this.exchange(0,i-1,e);
	        this.HeapAdjust(e, 1, i-1);
	    }
	}
	public void HeapAdjust(double[] e, int s, int t)
	{
		double tem = e[s-1];
	    for(int j = 2*s; j <= t; j *= 2)
	    {
	        if(j < t && e[j-1] < e[j])
	            j++;
	        if(tem >= e[j-1])
	            break;
	        e[s-1] = e[j-1];
	        s=j;
	    }
	    e[s-1]=tem;
	}
	
	
	// 插入排序
	public void Straight_Insertion_Sort(double[] e)       // 直接插入排序算法
	{
		double tmp;
		int j;
		for(int i = 1; i < e.length; i++)
	    {
	        tmp = e[i];
	        for(j = i; j > 0; j--)
	        {
	        	if(e[j-1] > tmp)
	        		e[j] = e[j-1];
	        	else
	        		break;
	        }
	        e[j]=tmp;
	    }
	}
	
	public void Shell_Sort(double[] e)                  // 希尔排序算法
	{
		double tpm;
		int k;
		for(int i =e.length / 2; i > 0; i /= 2)
	    {
	        for(int j = i; j < e.length; j++)
	        {
	            tpm = e[j];
	            for(k = j; k >= i; k -= i)
	            {
	            	if(e[k-i] > tpm)
	                    e[k]=e[k-i];
	                else
	                    break;
	            }
	            e[k]=tpm;
	        }
	        for(int j = 0; j < e.length; j++)
	        	System.out.print(e[j] + "   ");
	        System.out.println();
	    }
	}
	
	
	// 归并排序
	public void Merging_Sort(double[] e)          // 归并排序算法
	{
		double[] er = new double[e.length];
		this.MSort(e, er, 0, e.length-1);
	}
	public void MSort(double[] e, double[] er, int left, int right)
	{
		int center;
		if(left < right)
		{
			center = (left + right) / 2;
			this.MSort(e, er, left, center);
			this.MSort(e, er, center+1, right);
			this.Merge(e, er, left, center, right);
		}
	}
	public void Merge(double[] e, double[] er, int Lpre, int Lend, int Rend)
	{
		int Rpre = Lend + 1;
		int Tms = Lpre;
		int begin = Lpre;
		int end = Rend;
		while(Lpre <= Lend && Rpre <= Rend)
		{
			if(e[Lpre] <= e[Rpre])
				er[Tms++] = e[Lpre++];
			else
				er[Tms++] = e[Rpre++];
		}
		while(Lpre <= Lend)
			er[Tms++] = e[Lpre++];
		while(Rpre <= Rend)
			er[Tms++] = e[Rpre++];
		for(int i = begin; i <= end; i++)
			e[i] = er[i];
	}
	
	
	
	
	public void exchange(int a, int b, double[] e)
	{
		double ex  = e[a];
		e[a] = e[b];
		e[b] = ex;
	}
	
}
